<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/doc-types/list.php';

$action = get('action');
$id = get('id');
$sql;

switch ($action) {
    case 'delete':
        $sql = "DELETE FROM `doc_types` WHERE `doc_type_id`='{$id}'";
        break;
}

if (isset($sql)) {
    $db->query($sql);
    redirect($page_path);
}

if ($_POST) {
    $qr = $db->query("INSERT INTO `doc_types`(`doc_type_name`) VALUES ('{$_POST['doc_type_name']}')");
    
    if ($qr) {
        setAlert('success', "เพิ่มประเภทเอกสาร {$_POST['doc_type_name']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มประเภทเอกสาร {$_POST['doc_type_name']} ได้");
    }
    
    redirect($page_path);
}

$items = db_result("SELECT * FROM `doc_types`");

ob_start();
?>
<?= showAlert() ?>
<h3>เพิ่มประเภทเอกสาร</h3>
<form method="post">
    <label for="doc_type_name">ชื่อประเภทเอกสาร</label>
    <input type="text" name="doc_type_name" id="doc_type_name" required>
    <button type="submit">บันทึก</button>
</form>

<h3>รายการประเภทเอกสาร</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ชื่อประเภทเอกสาร</th>
            <th>จัดการประเภทเอกสาร</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['doc_type_id'] ?></td>
                <td><?= $item['doc_type_name'] ?></td>
                <td>
                    <a href="<?= urL("/admin/doc-types/edit.php?id={$item['doc_type_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['doc_type_id'] ?>"
                        <?= clickConfirm("คุณต้องการลบประเภทเอกสาร {$item['doc_type_name']} หรือไม่") ?>
                    >
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการประเภทเอกสาร';

require_once ROOT . '/admin/layout.php';
