<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = '/user/send/user.php';


if ($_POST) {
    $doc_file = upload('doc_file', '/storage/docs');
    if (!$doc_file) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดไฟล์เอกสารได้");
        redirect($page_path);
    }

    $qr = $db->query("INSERT INTO `docs`(
    `user_id`, 
    `doc_name`, 
    `doc_type_id`, 
    `doc_file`, 
    `send_type`, 
    `to_user_id`,  
    `read_status`, 
    `download`, 
    `send_time`) 
    VALUES (
    '{$user_id}',
    '{$_POST['doc_name']}',
    '{$_POST['doc_type_id']}',
    '{$doc_file}',
    'user',
    '{$_POST['send_id']}',
    0,
    0,
    NOW())");
    
    if ($qr) {
        setAlert('success', "ส่งเอกสาร {$_POST['doc_name']} สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถส่งเอกสาร {$_POST['doc_name']} ได้");
    }
    
    redirect($page_path);
}

$doc_types = db_result("SELECT * FROM `doc_types`");
$items = db_result("SELECT * FROM `users` WHERE `user_type`='user' AND `status`=1 AND `user_id`!='{$user_id}'");

ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="doc_name">ชื่อเอกสาร</label>
    <input type="text" name="doc_name" id="doc_name" required>
    <br>

    <label for="doc_file">ไฟล์เอกสาร</label>
    <input type="file" name="doc_file" id="doc_file" required>
    <br>

    <label for="doc_type_id">ประเภทเอกสาร</label>
    <select name="doc_type_id" id="doc_type_id" required>
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($doc_types as $item) : ?>
            <option value="<?= $item['doc_type_id'] ?>"><?= $item['doc_type_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    
    <label for="send_id">ส่งถึง</label>
    <select name="send_id" id="send_id" required>
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['user_id'] ?>"><?= $item['firstname'] . ' ' . $item['lastname'] ?> (<?= $item['email'] ?>)</option>
        <?php endforeach; ?>
    </select>
    <br>

    <button type="submit">ส่ง</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'ส่งเอกสารให้ผู้ใช้งานคนอื่น';

require_once ROOT . '/user/layout.php';
