<?php

return [
    'site_url' => 'http://skill65.local/docSend3',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_docSend3',
    'db_charset' => 'utf8mb4'
];
