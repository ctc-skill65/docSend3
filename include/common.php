<?php

function conf($key) {
    global $config;

    return isset($config[$key]) ? $config[$key] : null;
}

function url($path = '') {
    return conf('site_url') . $path;
}

function redirect($path = '') {
    header('Location: ' . url($path));
    exit;
}

function post($key) {
    return isset($_POST[$key]) ? $_POST[$key] : null;
}

function get($key) {
    return isset($_GET[$key]) ? $_GET[$key] : null;
}

function do_upload($name_file, $tmp_file, $dir = '/storage') {
    $path = $dir . '/';
    $path .= uniqid();
    $path .= '.';
    $path .= pathinfo($name_file, PATHINFO_EXTENSION);

    $full_path = ROOT . $path;

    move_uploaded_file($tmp_file, $full_path);
    if (is_file($full_path)) {
        return $path;
    }

    return false;
}

function upload($key, $dir = '/storage') {
    if (empty($_FILES[$key]['name']) || empty($_FILES[$key]['tmp_name'])) {
        return false;
    }

    return do_upload(
        $_FILES[$key]['name'],
        $_FILES[$key]['tmp_name'],
        $dir
    );
}

function upload_multi($key, $dir = '/storage') {
    if (empty($_FILES[$key]['name']) || empty($_FILES[$key]['tmp_name'])) {
        return false;
    }

    $data = [];

    foreach ($_FILES[$key]['name'] as $i => $file) {
        $path = do_upload(
            $_FILES[$key]['name'][$i],
            $_FILES[$key]['tmp_name'][$i],
            $dir
        );
        if ($path == false) {
            return false;
        }

        $data[] = $path;
    }

    return $data;
}

function setFlash($key, $value) {
    $_SESSION['flash'][$key] = $value;
}

function getFlash($key) {
    global $flash;

    if (isset($_SESSION['flash'][$key])) {
        return $_SESSION['flash'][$key];
    }

    return isset($flash[$key]) ? $flash[$key] : null;
}

function setAlert($status, $message) {
    setFlash('alert', [
        'status' => $status,
        'message' => $message
    ]);
}

function showAlert() {
    $alert = getFlash('alert');

    if (
        empty($alert) 
        || empty($alert['status'])
        || empty($alert['message'])
    ) {
        return;
    }

    $status = $alert['status'];
    $message = $alert['message'];

    ?>
    <script>
        window.onload = function () {
            setTimeout(() => {
                alert('<?= htmlspecialchars($message) ?>');
            }, 50);
        }
    </script>
    <?php
}

function clickConfirm($message) {
    return "onclick=\"return confirm('" . htmlspecialchars($message) . "')\"";
}
