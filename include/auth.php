<?php

function getUser($id) {
    return db_row("SELECT * FROM `users` WHERE `user_id`='{$id}'");
}

function checkLogin() {
    global $user_id, $user;

    $login_page = '/auth/login.php';

    if (empty($_SESSION['user_id'])) {
        redirect($login_page);
    }

    $user_id = $_SESSION['user_id'];
    $user = getUser($user_id);

    if (empty($user) || $user['status'] !== '1') {
        redirect($login_page);
    }
}

function checkAuth($user_type) {
    global $user;

    $login_page = '/auth/login.php';

    checkLogin();

    if ($user['user_type'] !== $user_type) {
        redirect($login_page);
    }
}
