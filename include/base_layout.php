<!DOCTYPE html>
<html lang="th">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= isset($page_name) ? $page_name . ' - ' : null ?>ระบบส่งเอกสารออนไลน์</title>
    <?= isset($layout_head) ? $layout_head : null ?>
</head>
<body>
    <h1>ระบบส่งเอกสารออนไลน์</h1>
    <hr>
    <?= isset($layout_body) ? $layout_body : null ?>
</body>
</html>