-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2023 at 02:42 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skill65_docsend3`
--

-- --------------------------------------------------------

--
-- Table structure for table `depts`
--

DROP TABLE IF EXISTS `depts`;
CREATE TABLE `depts` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `depts`
--

INSERT INTO `depts` (`dept_id`, `dept_name`) VALUES
(1, 'ฝ่ายการตลาด'),
(2, 'ฝ่ายขาย');

-- --------------------------------------------------------

--
-- Table structure for table `docs`
--

DROP TABLE IF EXISTS `docs`;
CREATE TABLE `docs` (
  `doc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `doc_name` varchar(60) NOT NULL,
  `doc_type_id` int(11) NOT NULL,
  `doc_file` varchar(60) NOT NULL,
  `send_type` enum('user','dept') NOT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `to_dept_id` int(11) DEFAULT NULL,
  `read_status` int(1) NOT NULL DEFAULT 0,
  `download` int(11) NOT NULL,
  `send_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `docs`
--

INSERT INTO `docs` (`doc_id`, `user_id`, `doc_name`, `doc_type_id`, `doc_file`, `send_type`, `to_user_id`, `to_dept_id`, `read_status`, `download`, `send_time`) VALUES
(1, 2, 'test', 5, '/storage/docs/63beadbadeb1d.json', 'user', 5, NULL, 0, 2, '2023-01-11 19:38:18'),
(2, 2, 'test2', 7, '/storage/docs/63beaef9c5e0d.jpg', 'dept', NULL, 1, 0, 0, '2023-01-11 19:43:37'),
(3, 5, 'test3', 5, '/storage/docs/63beaf3678471.jpg', 'dept', NULL, 2, 1, 1, '2023-01-11 19:44:38'),
(4, 5, 'test4', 4, '/storage/docs/63beaf4b999f2.jpg', 'user', 2, NULL, 1, 1, '2023-01-11 19:44:59');

-- --------------------------------------------------------

--
-- Table structure for table `doc_types`
--

DROP TABLE IF EXISTS `doc_types`;
CREATE TABLE `doc_types` (
  `doc_type_id` int(11) NOT NULL,
  `doc_type_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `doc_types`
--

INSERT INTO `doc_types` (`doc_type_id`, `doc_type_name`) VALUES
(4, 'เอกสารทั่วไป'),
(5, 'เอกสารฉบับร่าง'),
(6, 'เอกสารฉบับสมบูรณ์'),
(7, 'เอกสารต้นฉบับ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(25) DEFAULT NULL,
  `lastname` varchar(25) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `user_type` enum('admin','user') NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `email`, `password`, `dept_id`, `user_type`, `status`) VALUES
(1, 'admin', 'demo', 'admin@demo.com', '25d55ad283aa400af464c76d713c07ad', 0, 'admin', 1),
(2, 'user', 'demo', 'user@demo.com', '25d55ad283aa400af464c76d713c07ad', 2, 'user', 1),
(3, 'user2', 'demo', 'user2@demo.com', '25d55ad283aa400af464c76d713c07ad', 0, 'user', 1),
(5, 'user3', 'demo', 'user3@demo.com', '25d55ad283aa400af464c76d713c07ad', 1, 'user', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `depts`
--
ALTER TABLE `depts`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `docs`
--
ALTER TABLE `docs`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `doc_types`
--
ALTER TABLE `doc_types`
  ADD PRIMARY KEY (`doc_type_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `depts`
--
ALTER TABLE `depts`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `docs`
--
ALTER TABLE `docs`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `doc_types`
--
ALTER TABLE `doc_types`
  MODIFY `doc_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
